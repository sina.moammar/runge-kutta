'use strict';
$(document).ready(function () {
    let a, b, c, d, t0, tFinal, deltaT, stepNumber, output;
    let x, z, exact;

    function rungeKutta(stepNumber) {
        for (let i = 0; i < stepNumber.integerValue(); i++) {
            let k0 = deltaT.times(f(z[i])),
                i0 = deltaT.times(g(z[i], x[i])),
                k1 = deltaT.times(f(z[i].plus(i0.dividedBy(2)))),
                i1 = deltaT.times(g(z[i].plus(i0.dividedBy(2)), x[i].plus(k0.dividedBy(2)))),
                k2 = deltaT.times(f(z[i].plus(i1.dividedBy(2)))),
                i2 = deltaT.times(g(z[i].plus(i1.dividedBy(2)), x[i].plus(k1.dividedBy(2)))),
                k3 = deltaT.times(f(z[i].plus(i2))),
                i3 = deltaT.times(g(z[i].plus(i2), x[i].plus(k2)));

            x.push(x[i].plus(k0.plus(k1.times(2)).plus(k2.times(2)).plus(k3).dividedBy(6)));
            z.push(z[i].plus(i0.plus(i1.times(2)).plus(i2.times(2)).plus(i3).dividedBy(6)));
        }
    }

    function exactResults() {
        exact = [x[0]];
        for (let i = 0; i < stepNumber.integerValue(); i++) {
            let t = deltaT.times(i).plus(t0),
                sqrtThree = BigNumber(3).squareRoot();
            exact.push(BigNumber(1).dividedBy(sqrtThree).times(BigNumber(Math.sin(sqrtThree.times(t).toNumber()))).times(BigNumber(Math.exp(-1 * t.toNumber()))).plus(1));
            // exact.push()
        }
    }

    function printResult() {
        for (let i = 0; i <= stepNumber.toNumber(); i++) {
            output.html(output.html() + 'T = ' + (t0.plus(deltaT.times(i)).toNumber()) +
                '\n' +
                'X = ' +  x[i].toNumber() +
                '\n');
        }
    }

    function f(z) {
        return z;
    }

    function g(z, x) {
        return d.minus(c.times(x)).minus(b.times(z)).dividedBy(a);
    }

    $('#calc').click(function () {
        a = BigNumber($('#a').val());
        b = BigNumber($('#b').val());
        c = BigNumber($('#c').val());
        d = BigNumber($('#d').val());
        t0 = BigNumber($('#t0').val());
        tFinal = BigNumber($('#t_final').val());
        x = [BigNumber($('#x0').val())];
        z = [BigNumber($('#z0').val())];
        stepNumber = BigNumber($('#step_number').val());
        output = $('#out');
        output.html('');
        deltaT = tFinal.minus(t0).dividedBy(stepNumber);
        rungeKutta(stepNumber);
        exactResults();
        printResult();
    });
});