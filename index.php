<!DOCTYPE html>
<html>
<head>
    <script src="jquery-3.3.1.js"></script>
    <script src='bignumber.js-master/bignumber.js'></script>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="/mdb/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="/mdb/css/mdb.min.css" rel="stylesheet">
    <link rel="stylesheet" href="RungeKutta.css" type="text/css">
</head>
<body>
<div class="view background">
    <div class="image"></div>
    <div class="mask">
        <div class="container-fluid">
            <div class="row">
                <h1 class="mx-auto header mt-5">تمرین سری سوم مکانیک تحلیلی</h1>
            </div>
            <div class="row">
                <h5 class="mx-auto mt-1 mb-5">حل عددی معادله دیفرانسیل مرتبه دو به روش رانگ کوتا مرتبه ۴</h5>
            </div>
            <form class="row equation-row mx-auto my-5">
                <div class="col-auto">
                    <input type="number" id="a" class="form-control col"> <span> x'' + </span>
                </div>
                <div class="col-auto">
                    <input type="number" id="b" class="form-control col"> <span> x' + </span>
                </div>
                <div class="col-auto">
                    <input type="number" id="c" class="form-control col"> <span> x </span>
                </div>
                <div class="col-auto">
                    <span>= </span><input type="number" id="d" class="form-control col-lg">
                </div>
            </form>
            <div class="row equation-row mx-auto justify-content-around my-5">
                <div class="col-auto">
                    <span>t<sub>0</sub> = </span><input type="number" id="t0" class="form-control col">
                </div>
                <div class="col-auto">
                    <span>t<sub>f</sub> = </span><input type="number" id="t_final" class="form-control col">
                </div>
            </div>
            <div class="row equation-row mx-auto justify-content-around my-5">
                <div class="col-auto">
                    <span>x(t<sub>0</sub>) = </span><input type="number" id="x0" class="form-control col">
                </div>
                <div class="col-auto">
                    <span>x'(t<sub>0</sub>) = </span><input type="number" id="z0" class="form-control col">
                </div>
            </div>
            <div class="row equation-row mx-auto justify-content-around my-5">
                <div class="col-auto">
                    <span>Step Number = </span><input type="number" id="step_number" class="form-control col">
                </div>
                <div class="col-auto">
                    <button class="btn btn-primary" id="calc">Calculate!</button>
                </div>
            </div>
            <div class="row equation-row mx-auto justify-content-around my-5">
                <textarea class="col-12" id="out"></textarea>
            </div>
        </div>
    </div>
</div>


</body>
<script src="RungeKutta.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="/mdb/js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="/mdb/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="/mdb/js/mdb.min.js"></script>
</html>